var mongoose = require('mongoose');

var schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true
  }
};

var tradeSchema = new mongoose.Schema({
  askerId: String,
  ownerId: String,
  bookId: String,
  title: String,
  accepted: {type: Boolean, default: false},
  gone: {type: Boolean, default: false}
}, schemaOptions);

var Trade = mongoose.model('Trade', tradeSchema);

module.exports = Trade;
