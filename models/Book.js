var mongoose = require('mongoose');

var schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true
  }
};

var bookSchema = new mongoose.Schema({
  ownerId: String,
  title: String,
  author: String,
  coverUrl: String,
  description: String,
  traded: { type: Boolean, default: false }
}, schemaOptions);

var Book = mongoose.model('Book', bookSchema);

module.exports = Book;
