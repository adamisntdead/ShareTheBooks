var Trade = require('../models/Trade');
var Book = require('../models/Book');

exports.newTrade = function(req, res) {
    var trade = new Trade();

    trade.askerId = req.user.id;
    Book.findOne({
        _id: req.params.id
    }, function(err, book) {
       if (err) {
         console.log(err);
       }
       trade.ownerId = book.ownerId;
       trade.bookId = book._id;
       trade.title = book.title;

       trade.save(function(err){
         if (err) {
           console.log(err);
         }
         res.redirect('/shares');
       })
    });
};

exports.getTrades = function(req, res) {
  var sent;
  var recieved;

  Trade.find({
    askerId: req.user.id
  }, function(err, s){
    if (err) {
      console.log(err);
    }
    sent = s;
    Trade.find({ownerId: req.user.id, accepted: false, gone: false}, function(err, rec) {
      if (err) {
        console.log(err);
      }
      recieved = rec;
      res.render('shares', {
        title: 'Shares',
        sent: sent,
        recieved: recieved
      });
    });
  });
};

exports.acceptTrade = function(req, res) {
  Trade.findOne({_id: req.params.id}, function(err, trade) {
    trade.accepted = true;

    trade.save(function(err) {
      if (err) {
        console.log(err);
      }
      res.redirect('/shares');
    })
  });
};

exports.deleteTrade = function(req, res) {
  Trade.findOne({_id: req.params.id}, function(err, trade) {
    trade.gone = true;

    trade.save(function(err) {
      if (err) {
        console.log(err);
      }
      res.redirect('/shares');
    })
  });
};
