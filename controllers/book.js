var Book = require('../models/Book');
var User = require('../models/User');
var request = require('request');
var googleBooks = require('google-books-search');

var googleBooksOptions = {
    key: process.env.BOOKS_KEY
}

exports.postBook = function(req, res) {
    googleBooks.search(req.body.bookTitle, googleBooksOptions, function(error, results, apiResponse) {

        if (!error) {
            if (results.length != 0) {
                var book = new Book();
                book.ownerId = req.user.id;
                book.title = results[0].title;
                book.author = results[0].authors[0];
                book.coverUrl = results[0].thumbnail;
                book.description = results[0].description;

                book.save(function(err) {
                    if (err) {
                        console.log(err);
                    }
                    return res.redirect('/book/' + book._id);
                });
            } else {
                req.flash('error', {
                    msg: 'Couldn\'t Find That Book!'
                });
                return res.redirect('/add');
            }
        } else {
            console.log(error);
            res.redirect('/');
        }
    });
};

exports.getNew = function(req, res) {
    res.render('addBook', {
        title: 'Add Book'
    });
};

exports.getBookById = function(req, res) {
    Book.findOne({
        _id: req.params.id
    }, function(err, books) {
        if (err) {
            console.log(err);
            res.redirect('/');
        } else {
            if (typeof(books) == 'undefined' || books == null) {
                res.redirect('/');
            } else {
                User.findOne({
                    _id: books.ownerId
                }, function(err, user) {
                    if (err) {
                        console.log(err);
                        return res.redirect('/');
                    }
                    res.render('book', {
                        title: books.title,
                        book: books,
                        desc: books.description,
                        owner: user
                    });

                });
            }
        }
    });
}

exports.getMyBooks = function(req, res) {
    User.findOne({
        _id: req.user.id
    }, function(err, user) {
        if (err) {
            console.log(err);
        }
        Book.find({
            ownerId: req.user.id
        }, function(err, books) {
            if (err) {
                console.log(err);
            }
            res.render('userBooks', {
                title: user.name + '\'s Books',
                books: books,
                owner: user,
                mine: true
            })
        });

    });
};

exports.getUsersBooks = function(req, res) {
    User.findOne({
        _id: req.params.id
    }, function(err, user) {
        if (err) {
            console.log(err);
        }
        Book.find({
            ownerId: req.params.id
        }, function(err, books) {
            if (err) {
                console.log(err);
            }
            res.render('userBooks', {
                title: user.name + '\'s Books',
                books: books,
                owner: user
            })
        });

    });
}


exports.getAllBooks = function(req, res) {
    Book.find({}, function(err, results) {
        res.render('home', {
            title: 'Home',
            books: results
        })
    });
}
